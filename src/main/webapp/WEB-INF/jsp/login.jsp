<%@page import="java.util.ArrayList"%>
<%@page import="com.agencetrip.models.Offer"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!doctype html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="assets/css/index.css" type="text/css">
<title><%= request.getAttribute("title") %></title>
</head>
<body>
	<div class="content">
		<h1>Agencectrip</h1>
		<nav>
			<a href="/agencetrip">Accueil</a>
		</nav>
		<hr>
		<h2>Connexion</h2>
		<form action="login" method="post">
			<label>Alias</label>
			<input name="alias" type="text" value="<% if (request.getAttribute("alias") != null) out.print(request.getAttribute("alias"));  %>" required>
			<br>
			<br>
			<label>Mot de passe</label>
			<input name="password" type="password" required>
			<br>
			<input type="submit" value="Connexion">
		</form>
	</div>
</body>
</html>
