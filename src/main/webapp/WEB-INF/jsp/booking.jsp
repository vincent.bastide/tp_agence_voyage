<%@page import="java.util.ArrayList"%>
<%@page import="com.agencetrip.models.Offer"%>
<%@page import="com.agencetrip.models.Discount"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<% 
	Offer offer = (Offer) request.getAttribute("offer");
	@SuppressWarnings("unchecked")
	ArrayList<Discount> discounts = (ArrayList<Discount>) request.getAttribute("discounts");
%>
<!doctype html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="assets/css/index.css" type="text/css">
<title><%= request.getAttribute("title") %></title>
</head>
<body>
	<div class="content">
		<h1>Agencetrip</h1>
		<nav>
			<%
				if (request.getAttribute("isConnected") != null) {
					out.println("<a href=\"employee/logout\">Déconnexion</a>");
					out.println("<a href=\"employee/addoffer\">Ajouter une offre</a>");
					out.println("<a href=\"employee/addlocation\">Ajouter un lieu</a>");
				} else {
					out.println("<a href=\"login\">Connexion</a>");
					out.println("<a href=\"/agencetrip\">Accueil</a>");
				}
			%>
		</nav>
		 <hr>
		<h2>Réservation</h2>
		<div>
			<h3>Description</h3>
			<p><%= offer.getDescription() %></p>
			<h3>Lieu</h3>
			<p><% out.print(offer.getLocation().getName() + " " + offer.getLocation().getCountry()); %></p>
			<h4>Visites possibles</h4>
			<p><%= offer.getLocation().getVisits() %></p>
			<h4>Activités possibles</h4>
			<p><%= offer.getLocation().getActivities() %></p>
			<h3>Prix par personne</h3>
			<p id="price"><%= offer.getPricePerPerson() %>€</p>
			<label>Nombre de personne</label>
			<form action="booking" method="post">
				<input name="offer" type="hidden" value="<%= offer.getIdOffer() %>">
				<input id="nbPerson" name="nbPerson" type="number" min="1" max="<%= offer.getNbPersonMax() %>">
				<br>
				<%
					for(Discount discount : discounts) {
						out.print("<label>" + discount.getName() + "</label><br>");
						out.print("<input id=\"" + discount.getIdDiscount() + "\" name=\"" + discount.getName() + "\" type=\"number\" min=\"0\" max=\""+ offer.getNbPersonMax() + "\" value=\"0\">");
						out.print("<br>");
					}
				%>
				<br>
				<label>Email</label>
				<input type="email" required>
				<br>
				<h3>Prix total : <span id="priceToPay"></span></h3>
				<input id="submit" type="submit" value="Réserver">
			</form>
		</div>
	</div>
	<script>
		// objet remises
		const discounts = {
				underage: 0.5,
				adult: 1,
				senior: 0.7
		};

		// prix de l'offre
		const price =parseFloat(document.getElementById("price").textContent.replace("€", ""));

		// nb de personnes
		const nbPerson = document.getElementById("nbPerson").value;

		// Éléments HTML
		const $priceToPay = document.getElementById("priceToPay");
		const $form = document.querySelector("form");
		const $submit = document.getElementById("submit");

		// Fonctions
		const calculate = () => {
			const nbPerson = document.getElementById("nbPerson").value;
			let nbDiscounts = { // Nombre de remises
					underage: parseInt(document.getElementById("1").value),
					adult: parseInt(document.getElementById("2").value),
					senior: parseInt(document.getElementById("3").value)
				};
			nbDiscounts.tot = nbDiscounts.underage + nbDiscounts.adult + nbDiscounts.senior
			console.log(nbDiscounts.tot);
			let priceTot = 0;
			
			if (nbPerson < nbDiscounts.tot) {
				$priceToPay.textContent = "ERREUR, Nombre de personnes/Remises";
				$submit.disabled = true;
			} else if (nbDiscounts.tot < nbPerson) {
				$priceToPay.textContent = (price * nbPerson) + "€";
				$submit.disabled = true;
			} else if (nbDiscounts.tot == nbPerson) {
				$submit.disabled = false;
				
				if (nbDiscounts.underage > 0) {
					priceTot += (price * discounts.underage) * nbDiscounts.underage;
				}
				if (nbDiscounts.adult > 0) {
					priceTot += (price * discounts.adult) * nbDiscounts.adult;
				}
				if (nbDiscounts.senior > 0) {
					priceTot += (price * discounts.senior) * nbDiscounts.senior;
				}
				$priceToPay.textContent = priceTot + "€";
			}
		};

		$form.addEventListener('change', calculate); // À chaque changement dans le formulaire

		// Calcul initial
		calculate();
	</script>
</body>
</html>