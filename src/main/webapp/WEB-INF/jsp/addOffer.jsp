<%@page import="java.util.ArrayList"%>
<%@page import="com.agencetrip.models.Offer"%>
<%@page import="com.agencetrip.models.Location"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!doctype html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="assets/css/index.css" type="text/css">
<title><%= request.getAttribute("title") %></title>
</head>
<body>
	<div class="content">
		<h1>Agencetrip</h1>
		<nav>
			<%
				if (request.getAttribute("isConnected") != null) {
					out.println("<a href=\"logout\">Déconnexion</a>");
					out.println("<a href=\"/agencetrip\">Accueil</a>");
					out.println("<a href=\"addlocation\">Ajouter un lieu</a>");
				} else {
					out.println("<a href=\"login\">Connexion</a>");
				}
			%>
		</nav>
		 <hr>
		<h2>Ajouter une offre</h2>
		<form action="addoffer" method="post">
			<label>Lieu :</label>
			<select name="location" required>
				<option disabled selected>Veuillez saisir un lieu</option>
				<%
					@SuppressWarnings("unchecked")
					ArrayList<Location> locations = (ArrayList<Location>) request.getAttribute("locations");
					if (locations != null) {
						for (Location location : locations) {
							out.println("<option value=\""+ location.getIdLocation() + "\">" + location.getName() + "</option>");
						}
					}
				%>
			</select>
			<br>
			<label>Description</label>
			<br>
			<textarea name="description" rows="4" cols="40" required></textarea>
			<br>
			<label>Prix par personne</label>
			<input name="pricePerPerson" type="number" min="5" required>
			<br>
			<label>Nombre de personnes max</label>
			<input name="nbPersonMax" type="number" min="1" max="6" required>
			<br>
			<input type="submit" value="Ajouter">
		</form>
	</div>
</body>
</html>