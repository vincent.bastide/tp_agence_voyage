package com.agencetrip.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 
 * @author vincent
 *
 * Permet de modéliser une réservation
 * Implémente Serializable (Java Bean)
 *
 */
@Entity
@Table(name = "T_Booking")
public class Booking implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idBooking;
	
	@OneToOne
	@JoinColumn(name = "idOffer", nullable = false)
	private Offer offer;
	private int nbOfPerson;
	private double price;
	
	@ManyToMany
	@JoinTable(
			name = "T_Booking_Discounts_Associations",
			joinColumns = @JoinColumn(referencedColumnName = "idBooking"),
			inverseJoinColumns = @JoinColumn(referencedColumnName = "idDiscount"))
	private List<Discount> discounts = new ArrayList<Discount>();
	
	/**
	 * Constructeur par défaut nécessaire à Hibernate
	 */
	public Booking() {}

	/**
	 * Constructeur surchargé
	 * @param offer
	 * @param nbOfPerson
	 * @param price
	 */
	public Booking(Offer offer, int nbOfPerson, double price) {
		this.offer = offer;
		this.nbOfPerson = nbOfPerson;
		this.price = price;
	}

	/**
	 * Constructeur surchargé
	 * @param idBooking
	 * @param offer
	 * @param nbOfPerson
	 * @param price
	 */
	public Booking(int idBooking, Offer offer, int nbOfPerson, double price) {
		super();
		this.idBooking = idBooking;
		this.offer = offer;
		this.nbOfPerson = nbOfPerson;
		this.price = price;
	}

	/**
	 * Accesseur identifiant
	 * @return identifiant
	 */
	public int getIdBooking() {
		return idBooking;
	}

	/**
	 * Mutateur identifiant
	 * @param idBooking
	 */
	public void setIdBooking(int idBooking) {
		this.idBooking = idBooking;
	}

	/**
	 * Accesseur offre
	 * @return offre
	 */
	public Offer getOffer() {
		return offer;
	}

	/**
	 * Mutateur offer
	 * @param offer
	 */
	public void setOffer(Offer offer) {
		this.offer = offer;
	}

	/**
	 * Accesseur nombre de personnes
	 * @return nombre de personnes
	 */
	public int getNbOfPerson() {
		return nbOfPerson;
	}

	/**
	 * Mutateur nombre de personnes
	 * @param nbOfPerson
	 */
	public void setNbOfPerson(int nbOfPerson) {
		this.nbOfPerson = nbOfPerson;
	}

	/**
	 * Accesseur prix
	 * @return prix
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * Mutateur prix
	 * @param price
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * Accesseur remises
	 * @return liste des remises
	 */
	public List<Discount> getDiscounts() {
		return discounts;
	}

	/**
	 * Mutateur liste des remises
	 * @param discounts
	 */
	public void setDiscounts(List<Discount> discounts) {
		this.discounts = discounts;
	}
}
