package com.agencetrip.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * 
 * @author vincent
 *
 * Permet de modéliser une remise
 * Implémente Serializable (Java Bean)
 *
 */
@Entity
@Table(name = "T_category")
public class Discount implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idDiscount;
	private String name;
	private double discount;
	
	@ManyToMany
	@JoinTable(
			name = "T_Booking_Discounts_Associations",
			joinColumns = @JoinColumn(referencedColumnName = "idDiscount"),
			inverseJoinColumns = @JoinColumn(referencedColumnName = "idBooking"))
	private List<Booking> bookings = new ArrayList<Booking>();
	
	/**
	 * Constructeur par défaut nécessaire à Hibernate
	 */
	public Discount() {}

	/**
	 * Constructeur surchargé
	 * @param name
	 * @param discount
	 */
	public Discount(String name, double discount) {
		super();
		this.name = name;
		this.discount = discount;
	}

	/**
	 * Constructeur surchargé
	 * @param name
	 * @param discount
	 * @param bookings
	 */
	public Discount(String name, double discount, List<Booking> bookings) {
		super();
		this.name = name;
		this.discount = discount;
		this.bookings = bookings;
	}

	/**
	 * Accesseur identifiant
	 * @return identifiant
	 */
	public int getIdDiscount() {
		return idDiscount;
	}

	/**
	 * Mutateur identifiant
	 * @param idDiscount
	 */
	public void setIdDiscount(int idDiscount) {
		this.idDiscount = idDiscount;
	}

	/**
	 * Accesseur nom
	 * @return nom
	 */
	public String getName() {
		return name;
	}

	/**
	 * Mutateur nom
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Accesseur remise
	 * @return
	 */
	public double getDiscount() {
		return discount;
	}

	/**
	 * Mutateur remise
	 * @param discount
	 */
	public void setDiscount(double discount) {
		this.discount = discount;
	}

	/**
	 * Accesseur réservations
	 * @return liste des réservations
	 */
	public List<Booking> getBookings() {
		return bookings;
	}

	/**
	 * Mutateur liste des réservations
	 * @param bookings
	 */
	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}
}
