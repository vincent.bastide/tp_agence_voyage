package com.agencetrip.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * @author vincent
 *
 * Modélise un lieu
 * Implémente Serializable (Java Bean)
 *
 */
@Entity
@Table(name = "T_Location")
public class Location implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idLocation;
	private String name;
	private String country;
	private String visits;
	private String activities;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, targetEntity = Offer.class, mappedBy = "location")
	private List<Offer> offers = new ArrayList<Offer>();
	
	/**
	 * Constructeur par défaut nécessaire à Hibernate
	 */
	public Location() {}
	
	/**
	 * Constructeur surchargé
	 * @param name
	 * @param country
	 * @param visits
	 * @param activities
	 */
	public Location(String name, String country, String visits, String activities) {
		super();
		this.name = name;
		this.country = country;
		this.visits = visits;
		this.activities = activities;
	}

	/**
	 * Constructeur surchargé
	 * @param name
	 * @param country
	 * @param visits
	 * @param activities
	 * @param offers
	 */
	public Location(String name, String country, String visits, String activities, List<Offer> offers) {
		super();
		this.name = name;
		this.country = country;
		this.visits = visits;
		this.activities = activities;
		this.offers = offers;
	}

	/**
	 * Constructeur surchargé
	 * @param idLocation
	 * @param name
	 * @param country
	 * @param visits
	 * @param activities
	 * @param offers
	 */
	public Location(int idLocation, String name, String country, String visits, String activities, List<Offer> offers) {
		super();
		this.idLocation = idLocation;
		this.name = name;
		this.country = country;
		this.visits = visits;
		this.activities = activities;
		this.offers = offers;
	}

	/**
	 * Accesseur identifiant
	 * @return identifiant
	 */
	public int getIdLocation() {
		return idLocation;
	}

	/**
	 * Mutateur identifiant
	 * @param idLocation
	 */
	public void setIdLocation(int idLocation) {
		this.idLocation = idLocation;
	}
	
	/**
	 * Accesseur nom
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Mutateur nom
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Accesseur pays
	 * @return
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Mutateur pays
	 * @param country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Accesseur visites
	 * @return
	 */
	public String getVisits() {
		return visits;
	}

	/**
	 * Mutateur visites
	 * @param visits
	 */
	public void setVisits(String visits) {
		this.visits = visits;
	}

	/**
	 * Accesseur activités
	 * @return
	 */
	public String getActivities() {
		return activities;
	}
	
	/**
	 * Mutateur activités
	 * @param activities
	 */
	public void setActivities(String activities) {
		this.activities = activities;
	}

	/**
	 * Accesseur offres associées
	 * @return liste des offres
	 */
	public List<Offer> getOffers() {
		return offers;
	}

	/**
	 * Mutateur liste des offres
	 * @param offers
	 */
	public void setOffers(List<Offer> offers) {
		this.offers = offers;
	}
}
