package com.agencetrip.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 
 * @author vincent
 *
 * Modélise une offre
 * Implémente Serializable (Java Bean)
 *
 */
@Entity
@Table(name = "T_Offer")
public class Offer implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idOffer;
	private String description;
	private double pricePerPerson;
	private int nbPersonMax;
	
	@ManyToOne
	@JoinColumn(name = "idLocation", nullable = false)
	private Location location;
	
	@OneToOne(mappedBy = "offer")
	private Booking booking;
	
	/**
	 * Constructeur par défaut nécessaire à Hibernate
	 */
	public Offer() {}
	
	/**
	 * Constructeur surchargé
	 * @param description
	 * @param pricePerPerson
	 * @param nbPersonMax
	 * @param location
	 */
	public Offer(String description, double pricePerPerson, int nbPersonMax, Location location) {
		super();
		this.description = description;
		this.pricePerPerson = pricePerPerson;
		this.nbPersonMax = nbPersonMax;
		this.location = location;
	}

	/**
	 * Contsructeur surchargé
	 * @param description
	 * @param pricePerPerson
	 * @param nbPersonMax
	 * @param location
	 * @param booking
	 */
	public Offer(String description, double pricePerPerson, int nbPersonMax, Location location, Booking booking) {
		super();
		this.description = description;
		this.pricePerPerson = pricePerPerson;
		this.nbPersonMax = nbPersonMax;
		this.location = location;
		this.booking = booking;
	}

	/**
	 * Constructeur surchargé
	 * @param idOffer
	 * @param description
	 * @param pricePerPerson
	 * @param nbPersonMax
	 * @param location
	 * @param booking
	 */
	public Offer(int idOffer, String description, double pricePerPerson, int nbPersonMax, Location location, Booking booking) {
		super();
		this.idOffer = idOffer;
		this.description = description;
		this.pricePerPerson = pricePerPerson;
		this.nbPersonMax = nbPersonMax;
		this.location = location;
		this.booking = booking;
	}

	/**
	 * Accesseur identifiant
	 * @return identifiant
	 */
	public int getIdOffer() {
		return idOffer;
	}

	/**
	 * Mutateur identifiant
	 * @param idOffer
	 */
	public void setIdOffer(int idOffer) {
		this.idOffer = idOffer;
	}

	/**
	 * Accesseur description
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Mutateur description
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Accesseur prix par personne
	 * @return prix par personne
	 */
	public double getPricePerPerson() {
		return pricePerPerson;
	}

	/**
	 * Mutateur prix par personne
	 * @param pricePerPerson
	 */
	public void setPricePerPerson(double pricePerPerson) {
		this.pricePerPerson = pricePerPerson;
	}

	/**
	 * Accesseur nombre de personnes max
	 * @return nombre de personnes max
	 */
	public int getNbPersonMax() {
		return nbPersonMax;
	}

	/**
	 * Mutateur nombre de personnes max
	 * @param nbPersonMax
	 */
	public void setNbPersonMax(int nbPersonMax) {
		this.nbPersonMax = nbPersonMax;
	}

	/**
	 * Accesseur lieu associé
	 * @return lieu
	 */
	public Location getLocation() {
		return location;
	}

	/**
	 * Mutateur lieu
	 * @param location
	 */
	public void setLocation(Location location) {
		this.location = location;
	}

	/**
	 * Accesseur réservation
	 * @return réservation
	 */
	public Booking getBooking() {
		return booking;
	}

	/**
	 * Mutateur réservation
	 * @param booking
	 */
	public void setBooking(Booking booking) {
		this.booking = booking;
	}
}
