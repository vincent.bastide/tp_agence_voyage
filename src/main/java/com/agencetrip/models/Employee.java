package com.agencetrip.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author vincent
 *
 * Modélise un employée
 * Implémente Serializable (Java Bean)
 *
 */
@Entity
@Table(name = "T_employees")
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idEmployee;
	private String lastname;
	private String firstname;
	private int age;
	private String address;
	private String alias;
	private String password;
	
	/**
	 * Constructeur par défaut nécessaire à Hibernate
	 */
	public Employee() {}
	
	/**
	 * Contructeur surchargé
	 * @param lastname
	 * @param firstname
	 * @param age
	 * @param address
	 * @param alias
	 * @param password
	 */
	public Employee(String lastname, String firstname, int age, String address, String alias, String password) {
		super();
		this.lastname = lastname;
		this.firstname = firstname;
		this.age = age;
		this.address = address;
		this.alias = alias;
		this.password = password;
	}

	/**
	 * Constructeur surchargé
	 * @param idEmployee
	 * @param lastname
	 * @param firstname
	 * @param age
	 * @param address
	 * @param alias
	 * @param password
	 */
	public Employee(int idEmployee, String lastname, String firstname, int age, String address, String alias,
			String password) {
		super();
		this.idEmployee = idEmployee;
		this.lastname = lastname;
		this.firstname = firstname;
		this.age = age;
		this.address = address;
		this.alias = alias;
		this.password = password;
	}

	/**
	 * Accesseur identifiant
	 * @return identifiant
	 */
	public int getIdEmployee() {
		return idEmployee;
	}

	/**
	 * Mutateur identifiant
	 * @param idEmployee
	 */
	public void setIdEmployee(int idEmployee) {
		this.idEmployee = idEmployee;
	}

	/**
	 * Accesseur nom de famille
	 * @return nom de famille
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * Mutateur nom de famille
	 * @param lastname
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * Accesseur prénom
	 * @return prénom
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * Mutateur prénom
	 * @param firstname
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * Accesseur alias
	 * @return alias
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Mutateur alias
	 * @param alias
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * Accesseur âge
	 * @return âge
	 */
	public int getAge() {
		return age;
	}

	/**
	 * Mutateur âge
	 * @param age
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * Accesseur adresse
	 * @return
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Mutateur adresse
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Accesseur mot de passe
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Mutateur mot de passe
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
}
