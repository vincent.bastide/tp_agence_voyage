package com.agencetrip.listeners;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletRequest;

/**
 * 
 * @author vincent
 *
 * Permet de gérer des actions à chaque requête
 *
 */
@WebListener
public class RequestListener implements ServletRequestListener {
	/**
	 * Ajoute un attribut "isConnected" si l'utilisateur est connecté
	 */
    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent) {
        HttpServletRequest request = (HttpServletRequest) servletRequestEvent.getServletRequest();
        if (request.getSession(false) != null) {
            Integer isUserConnected = (Integer) request.getSession(false).getAttribute("employee_id");
            request.setAttribute("isConnected", isUserConnected);
        }
    }

    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
    }
}