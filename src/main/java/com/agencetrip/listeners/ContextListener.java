package com.agencetrip.listeners;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.agencetrip.dao.DAOSingleton;
import com.agencetrip.models.Discount;
import com.agencetrip.models.Employee;
import com.agencetrip.models.Location;
import com.agencetrip.models.Offer;

/**
 * 
 * @author vincent
 *
 * Permet de faire des actions au démarrage du context et à son arrêt
 *
 */
@WebListener
public class ContextListener implements ServletContextListener {
	/**
	 * Création de données par défaut au démarrage du contexte
	 * Démarre le Singleton EntityManagerFactory
	 * @see DAOSingleton
	 */
	public void contextInitialized(ServletContextEvent sce) {
		EntityManager entityManager = null;
		Employee employee = null;
		Location location = null;
		Offer offer = null;
		List<Discount> discounts = null;
		
		try {
			entityManager = DAOSingleton.getEntityManagerFactory().createEntityManager();
			employee = new Employee("ROOT", "ROOT", 42, "localhost", "root", "pass");
			location = new Location("Donostia", "Espagne", "Vieille ville / Ile Santa Clara", "Randonnée / Bateau / Parc d'attraction / Aquarium");
			offer = new Offer("Ville du pays Basque Espagnol", 115, 3, location);
			discounts = new ArrayList<Discount>();
			
			discounts.add(new Discount("Mineur", 0.5));
			discounts.add(new Discount("Majeur", 1));
			discounts.add(new Discount("Senior", 0.7));
			
			
			// Ajout utilisateur par defaut / Offre / lieu / Remises
			EntityTransaction transaction = entityManager.getTransaction();
			transaction.begin();
			
			for (Discount discount : discounts) {
				entityManager.persist(discount);
			}
			
			entityManager.persist(employee);
			entityManager.persist(location);
			entityManager.persist(offer);
			
			transaction.commit();
		} finally {
			if (entityManager != null) {
				entityManager.close();
			}
		}
	}

	/**
	 * Ferme le singleton
	 */
	public void contextDestroyed(ServletContextEvent sce) {
		DAOSingleton.closeEntityManagerFactory();
	}
}
