package com.agencetrip.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.agencetrip.dao.DiscountDAO;
import com.agencetrip.dao.InsertDAO;
import com.agencetrip.dao.OfferDAO;
import com.agencetrip.models.Booking;
import com.agencetrip.models.Discount;
import com.agencetrip.models.Offer;

/**
 * 
 * @author vincent
 *
 * Permet de gérer l'ajout d'une réservation
 *
 */
@WebServlet(urlPatterns = "/booking")
public class AddBooking extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Test de l'existence du paramètre "offerId"
		Integer offerId = (!request.getParameter("offerId").isEmpty()) ? Integer.parseInt(request.getParameter("offerId")) : null;
		Offer offer = null;
		List<Discount> discounts = null;
		
		// Si il y a bien une offre on ajoute des attributs pour la jsp
		if (offerId != null) {
			offer = new OfferDAO().find(offerId);
			discounts = new DiscountDAO().getAll();
			request.setAttribute("offer", offer);
			request.setAttribute("discounts", discounts);
		}
		
		request.setAttribute("title", "Reservation");
		this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/booking.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Test de l'existence des paramètres
		Integer offerId = (!request.getParameter("offer").isEmpty()) ? Integer.parseInt(request.getParameter("offer")) : null;
		Integer nbPerson = (!request.getParameter("nbPerson").isEmpty()) ? Integer.parseInt(request.getParameter("nbPerson")) : null;
		Integer underage = (!request.getParameter("Mineur").isEmpty()) ? Integer.parseInt(request.getParameter("Mineur")) : null;
		Integer adult = (!request.getParameter("Majeur").isEmpty()) ? Integer.parseInt(request.getParameter("Majeur")) : null;
		Integer senior = (!request.getParameter("Senior").isEmpty()) ? Integer.parseInt(request.getParameter("Senior")) : null;
		List<Discount> discounts = null;
		Offer offer = null;
		double price = 0;
		
		// Si les paramètres existent, sauvegarde de la réservation en base de données
		if (offerId != null && nbPerson != null && underage != null && adult != null && senior != null) {
			 discounts = new DiscountDAO().getAll();
			 offer = new OfferDAO().find(offerId);
			 
			 // Si le nombre de personne est inférieur au nombre de remise retour à l'accueil
			 if (nbPerson < (underage + adult + senior)) {
				 response.sendRedirect(request.getContextPath() + "/");
			 // Si le nombre de personne est supérieure au nombre de remise retoru à l'accueil
			 } else if ((underage + adult + senior) < nbPerson) {
				 response.sendRedirect(request.getContextPath() + "/");;
			 }
			 
			 // Calcul du prix en fonction des tarifs par tranche d'âge
			 for (Discount discount : discounts) {
				 if (discount.getName().equals("Mineur") && underage > 0) {
					 price += (offer.getPricePerPerson() * discount.getDiscount()) * underage;
				 } else if (discount.getName().equals("Majeur") && adult > 0) {
					 price += (offer.getPricePerPerson() * discount.getDiscount()) * adult;
				 } else {
					 price += (offer.getPricePerPerson() * discount.getDiscount()) * senior;
				 }
			 }
			 
			 // Sauvegarde dans la base de données
			 new InsertDAO().saveObject(new Booking(offer, nbPerson, price));
			 response.sendRedirect("payment?price=" + price);
		} else {
			response.sendRedirect(request.getContextPath() + "/");
		}
	}
}
