package com.agencetrip.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 
 * @author vincent
 *
 * Permet de gérer le paiement d'une réservation
 *
 */
@WebServlet(urlPatterns = "/payment")
public class Payment extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Test de l'existence du paramètre
		Float price = (!request.getParameter("price").isEmpty()) ? Float.parseFloat(request.getParameter("price")) : null;
		
		request.setAttribute("title", "paiement");
		request.setAttribute("price", price);
		this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/payment.jsp").forward(request, response);
	}
}
