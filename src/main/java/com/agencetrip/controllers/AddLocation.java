package com.agencetrip.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.agencetrip.dao.InsertDAO;
import com.agencetrip.models.Location;

/**
 * 
 * @author vincent
 *
 * Permet de gérer l'ajout d'un lieu
 *
 */
@WebServlet(urlPatterns = "/employee/addlocation")
public class AddLocation extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("title", "Ajouter lieu");
		this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/addLocation.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Test de l'existence des paramètres
		String name = (!request.getParameter("name").isEmpty()) ? request.getParameter("name") : null;
		String country = (!request.getParameter("country").isEmpty()) ? request.getParameter("country") : null;
		String visits = (!request.getParameter("visits").isEmpty()) ? request.getParameter("visits") : null;
		String activities = (!request.getParameter("activities").isEmpty()) ? request.getParameter("activities") : null;
		Location location = null;
		
		if (name != null & country != null && visits != null && activities != null) {
			location = new Location(name, country, visits, activities);
			
			// Sauvegarde du lieu dans la base de données
			new InsertDAO().saveObject(location);
			
			// Redirection vers la page d'accueil
			response.sendRedirect(request.getContextPath() + "/");
		} else {
			// Réaffiche la page d'ajout d'un lieu s'il manque un paramètre
			request.setAttribute("title", "Ajouter lieu");
			this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/addLocation.jsp").forward(request, response);
		}
	}

}
