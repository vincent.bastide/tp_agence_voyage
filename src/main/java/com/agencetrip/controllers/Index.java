package com.agencetrip.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.agencetrip.dao.OfferDAO;
import com.agencetrip.models.Offer;

/**
 * 
 * @author vincent
 *
 * Gère la page d'accueil
 *
 */
@WebServlet(urlPatterns = "")
public class Index extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		OfferDAO offerDAO = new OfferDAO();
		List<Offer> offers = offerDAO.getAll();
		
		request.setAttribute("title", "Accueil");
		request.setAttribute("offers", offers);
		this.getServletContext().getRequestDispatcher("/WEB-INF/jsp/index.jsp").forward(request, response);
	}
}
