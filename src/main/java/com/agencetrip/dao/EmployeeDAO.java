package com.agencetrip.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import com.agencetrip.models.Employee;

/**
 * 
 * @author vincent
 *
 * Permet de gérer les actions CRUD pour l'objet Employee
 * 
 * @see Employee
 *
 */
public class EmployeeDAO {
	/**
	 * Permet de vérifier les identifiants
	 * 
	 * @param alias
	 * @param password
	 * @return L'objet employee qui correspond aux ids | null sinon
	 */
	public Employee checkCredentials(String alias, String password) {
		Employee employee = null;
		EntityManager entityManager = null;

		try {
			entityManager = DAOSingleton.getEntityManagerFactory().createEntityManager();

			EntityTransaction transaction = entityManager.getTransaction();
			transaction.begin();
			employee = entityManager.createQuery("from Employee WHERE alias = :alias", Employee.class)
					.setParameter("alias", alias).getSingleResult();
			transaction.commit();

			if ((employee != null) && !employee.getPassword().equals(password)) {
				employee = null;
			}
		} finally {
			if (entityManager != null) {
				entityManager.close();
			}
		}
		return employee;
	}
}
