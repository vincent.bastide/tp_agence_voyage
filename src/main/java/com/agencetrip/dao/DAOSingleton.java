package com.agencetrip.dao;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * 
 * @author vincent
 *
 * Permet de gérer la factory pour créer des EntityManager
 * @see EntityManagerFactory
 *
 */
public class DAOSingleton {
	private static EntityManagerFactory entityManagerFactory;
	
	/**
	 * Création de l'instance EntityManagerFactory
	 */
	static {
		try {
			entityManagerFactory = Persistence.createEntityManagerFactory("agencetrip");
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * 
	 * @return l'instance de EntityManagerFactory
	 */
	public static EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}
	
	/**
	 * Ferme l'EntityManagerFactory
	 */
	public static void closeEntityManagerFactory() {
		if (entityManagerFactory != null) {
			entityManagerFactory.close();
		}
	}
}
